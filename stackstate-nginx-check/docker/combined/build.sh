#!/usr/bin/env bash

mkdir -p tmp/etc/nginx/
mkdir -p tmp/nginxmetrics/checks.d/
mkdir -p tmp/nginxmetrics/conf.d/
mkdir -p tmp/nginxtopo/checks.d/
mkdir -p tmp/nginxtopo/conf.d/
cp ../../nginx/stackstate_checks/nginxtopo/data/conf.yaml.example ./tmp/nginxtopo/conf.d/conf.yaml
cp ../../nginx/stackstate_checks/nginxtopo/*.py ./tmp/nginxtopo/checks.d
cp ../../nginx/tests/data/demo/* ./tmp/etc/nginx 
cp ../../nginx/stackstate_checks/nginxmetrics/data/conf.yaml.docker.example ./tmp/nginxmetrics/conf.d/conf.yaml
cp ../../nginx/stackstate_checks/nginxmetrics/*.py ./tmp/nginxmetrics/checks.d

docker build -t stackstate/stsnginxtopo:1.0.2 .
