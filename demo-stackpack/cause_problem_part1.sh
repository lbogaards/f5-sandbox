#!/usr/bin/env bash

set -e

# update customerAservice with the version 6.0
sts topology send < ./templates/15paymentservice6.yaml

# Adding switches - specific to Telco scenario
sts topology send < ./templates/18switchesadd.yaml || true

echo sending 40 events
for x in {0..40}; do
    sts event send slow_query -l slow_query -x "Time_0.1s_Lock_0.00s_Rows_3_(1404),_SELECT_ROUND(COUNT(*)/N,N)_AS_rank,_tag,_COUNT(*)_as_cnt_FROM_pixelpost_pixelpost_AS_p,_pixelpost_traffic_AS_t_WHERE_t.alt_tag_LIKE_'%TRAFFIC%'_AND_p.id_=_t.img_id_AND_p.datetime<='S'_ORDER_BY_tag" -t user:service@stackstate.com
    sts event send error_log -l error_log -x "[error_500]_11154#11154:_*118 open_home/ATUwebshop/stylplus/static/checkout.css_failed__client:_88.128.81.132_server:_67.207.87.22_request:_GET_/static/base.css_HTTP/1.1_host:_67.207.87.22_referrer:_http://67.207.87.22/account/" -t user:service@stackstate.com
done

for x in {0..5}; do
    sts metric send Class3_Receive_Timeout_Frame_Rate -p 10s -r 5 -b 91-97
    sleep 1
    sts metric send Discarded_Frame_Rate -p 10s -r 5 -b 91-97
    sleep 1
done

for x in {0..2}; do
    sts metric send db.mysql.full_table_scans  -p 10s -r 5 -b 98-102
    sleep 1
done
