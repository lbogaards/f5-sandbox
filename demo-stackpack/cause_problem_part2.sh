#!/usr/bin/env bash

set -e

for x in {0..3}; do
    sts metric send http.response_time -p 10s -r 5 -b 975-1043
    sleep 1
    sts metric send nginx.responses.5xx  -p 10s -r 5 -b 240-260
    sleep 1
    sts metric send apache.responses.5xx  -p 10s -r 5 -b 180-210
    sleep 1
done

sts metric send active_users.second -p 10m -b 30-40
sleep 1
