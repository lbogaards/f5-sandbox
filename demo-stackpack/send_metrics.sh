#!/usr/bin/env bash

set -e

# update customerAservice with the version 5.0
sts topology send < ./templates/15paymentservice.yaml

sts metric send active_users.second --baseline -b 300-350 --noise 30 -p 10m

sts metric send traffic --baseline -b 500-850 -p 10m
sts metric send db.mysql.full_table_scans -p 10m -r 5 -b 2-3
sts metric send Dtu_consumption_percentage -p 10m -r 5 -b 2-3
sleep 1
sts metric send Dtu_consumption_percentage 6
sts metric send Dtu_consumption_percentage 8
sts metric send Dtu_consumption_percentage -p 10m -r 5 -b 10-23
sleep 1
sts metric send http.response_time -p 10m -r 5 -b 45-103
sleep 1
sts metric send average_response_time --baseline -b 89-93 -p 10m
sleep 1
sts metric send Class3_Receive_Timeout_Frame_Rate --baseline -b 02-04 -p 10m
sts metric send Discarded_Frame_Rate --baseline -b 01-03 -p 10m
sts metric send cpu_iowait --baseline -b 10-27 -p 10m
sleep 1
sts metric send heap_usage --linear -b 30-40 -p 10m --noise 3
sleep 1
sts metric send garbage_collection_cycles_per_min --linear -b 2-5 -p 10m --noise 1
sleep 1
sts metric send nginx.responses.5xx --linear -b 2-8 -p 10m --noise 1
sts metric send apache.responses.5xx --linear -b 2-8 -p 10m --noise 1
sleep 1
sts metric send restartcount_per_hour --linear -b 1-2 -p 10m --noise 1
sleep 1
sts metric send unhealthy_host_count 0.001
sleep 1
sts metric send unhealthy_host_count 0.001
sleep 1
sts metric send unhealthy_host_count 0.001
sleep 1
sts metric send Open_app_duration --linear -b 12-21 -p 10m --noise 1
sleep 1
sts metric send Login_duration --linear -b 8-19 -p 10m --noise 1
sleep 1
