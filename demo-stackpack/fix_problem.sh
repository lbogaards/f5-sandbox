#!/usr/bin/env bash

set -e

# update customerAservice with the version 5.0
sts topology send < ./templates/15paymentservice.yaml

echo sending metrics every 5 seconds

for x in {0..3}; do
    sts metric send Class3_Receive_Timeout_Frame_Rate -p 10s -r 5 -b 3-5
    sleep 2
    sts metric send Discarded_Frame_Rate -p 10s -r 5 -b 1-3
    sleep 2
    sts metric send db.mysql.full_table_scans -p 5s -r 1 -b 2-4
    sleep 2
    sts metric send http.response_time -p 5s -r 1 -b 68-101
    sleep 2
done

sts metric send nginx.responses.5xx  -p 10s -r 5 -b 2-8
sts metric send apache.responses.5xx  -p 10s -r 5 -b 2-8
sleep 4

sts metric send active_users.second --baseline -b 300-350 --noise 30 -p 20m
sleep 10


# Adding switches - specific to Telco scenario
sts topology send < ./templates/del_comps.yaml || true
